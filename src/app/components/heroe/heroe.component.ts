import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HeroesService, Heroe} from '../../servicios/heroes.service';


@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html'
})
export class HeroeComponent implements OnInit {

  heroe:Heroe;

  constructor(private activatedRoute: ActivatedRoute, private _heroeService: HeroesService, private router: Router) {
    this.activatedRoute.params.subscribe(params => {
      console.log(params['id']);
      this.heroe = this._heroeService.getHeroe(params['id']);
    });
  }

  ngOnInit() {
  }

}
